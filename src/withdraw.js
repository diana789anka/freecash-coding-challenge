const { default: WithdrawService } = require('./services/WithdrawService');
const { default: UserService } = require('./services/UserService');
const { default: Socket } = require('./lib/socket');

module.exports = {
  onsiteGiftcardWithdraw: function (socket, socketuser) {
    return function (data) {
      if (!data) return feedback(`An unknown error occurred`);
      const withdrawService = new WithdrawService(data);
      const userService = new UserService();
      const socketObj = new Socket(socket, socketuser);
      try {
        await withdrawService.withdrawValidate();
        await userService.userValidate(socketuser.gainid, withdrawService.coinAmount);
      } catch (err) {
        if (err.handled) return socketObj.feedback(err.message);
        console.error(err);
        return socketObj.feedback('An error occured');
      }
      try {
        (await userService.isUserVerified())
          ? getGiftCardVerify(withdrawService)
          : getGiftCardInStock(withdrawService, socketObj);
      } catch (err) {
        if (err.handled) return socketObj.feedback(err.message);
        console.error(err);
        return socketObj.feedback('An error occured');
      }
    };
  },

  getGiftCardInStock: function (withdrawService, socketObj) {
    try {
      const result = await GiftcardService.isGiftCardInStock(
        withdrawService.type,
        withdrawService.coinAmount,
        withdrawService.countryCode
      );
      if (result)
        return (
          (await withdrawService.notVerified()) &&
          socketObj.notVerfiedFeedback(outOfStock, {
            type: withdrawService.type,
            coinAmount: withdrawService.coinAmount,
          })
        );

      return socketObj.feedback(`This card is currently out of stock. Please choose another.`);
    } catch (err) {
      console.error(err);
      return withdrawService.notVerified() && socketObj.notVerfiedFeedback(outOfStock);
    }
  },

  getGiftCardVerify: function (withdrawService) {
    try {
      const date = Date.now();
      const giftcardCode = await GiftcardService.getGiftcard(
        withdrawService.type,
        coinPrice,
        withdrawService.countryCode
      );
      if (giftcardCode)
        return (
          (await withdrawService.verfied(giftcardCode, date)) &&
          socketObj.VerfiedFeedback(withdrawService.coinAmount)
        );
      return socketObj.feedback(`This gift card in unavailable`);
    } catch (err) {
      console.error(err);
      throw new GiftCardUnavailableError();
    }
  },
};
