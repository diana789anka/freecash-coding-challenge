export function throwHandledError(errorMessage) {
  throw new Error(errorMessage, { handled: true });
}
