let feedbackType = '';

export default class SocketClass {
  constructor(socket, socketuser) {
    this.socket = socket;
    this.socketuser = socketuser;
  }

  feedback = (feedback) => {
    socket.emit('withdrawFeedback', feedback, feedbackType);
  };

  notVerfiedFeedback = (outOfStock, data) => {
    this.socket.emit('withdrawalPending', {
      coins: data.coinAmount,
    });

    Notifications.storeNotification(
      this.socketuser.gainid,
      'Info',
      'pendingwithdrawal',
      `Your ${data.type} Gift Card withdrawal worth ${data.coinAmount} coins is pending.`
    );

    if (outOfStock) {
      feedback(
        `Success!<br>This card is currently out of stock. A staff member will approve your withdrawal when it is restocked.`
      );
    } else {
      feedback(
        `Success!<br>Since you are not verified, a staff member has been notified and will review your withdrawal shortly! Check your Profile page to view your redemption code after the withdrawal has been approved.<br><br>Have an opinion on our site? Share it by <a href="https://trustpilot.com/evaluate/freecash.com" target="_blank">writing a Trustpilot review</a>!`
      );
    }

    emitBalance(this.socketuser.gainid);
  };

  verfiedFeedback = (coinAmount) => {
    Notifications.storeNotification(
      this.socketuser.gainid,
      'Info',
      'gift card',
      `You have a Gift Card withdrawal worth ${coinAmount} coins.`
    );
    feedback(`Success!<br>You have a Gift Card withdrawal worth ${coinAmount} coins.`);
    emitBalance(this.socketuser.gainid);
  };
}
