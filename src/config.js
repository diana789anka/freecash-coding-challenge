const config = {
  withdraw: {
    minEarnedToWithdraw: 200,
    validateTypes: ['Fortnite', 'Visa', 'Amazon', 'Steam', 'Roblox'],
    validateCountryCodes: ['US', 'UK', 'CA', 'DE', 'FR', 'AU', 'WW'],
  },
};

module.exports = config;
