const mysql = require('mysql');
const config = require('./config');
const assert = require('better-assert');
const connection = await mysql.createPool(config.database);

const db = {
  getAccountStanding: async function (gainid) {
    let sql = `
      SELECT * FROM banned WHERE gainid = ?;
      SELECT * FROM frozen WHERE gainid = ?;
      SELECT * FROM muted WHERE gainid = ?;
      SELECT * FROM countrybanned WHERE gainid = ?;
      SELECT deleted FROM users WHERE gainid = ?;
    `;
    try {
      const result = await connection.query(sql, [gainid, gainid, gainid, gainid, gainid, gainid]);
      let accountStanding = {
        banned: false,
        frozen: false,
        muted: false,
        countryBanned: false,
        deleted: false,
      };
      if (result[0].length) accountStanding.banned = true;
      if (result[1].length) accountStanding.frozen = true;
      if (result[2].length) accountStanding.muted = true;
      if (result[3].length) accountStanding.countryBanned = true;
      if (result[4][0].deleted) accountStanding.deleted = true;

      return accountStanding;
    } catch (err) {
      throw new Error(err.message);
    }
  },
  isDefaultUserEmailVerified: async function (gainid) {
    const sql = `
        SELECT email_confirmed FROM defaultusers WHERE gainid = ?`;
    try {
      const result = await connection.query(sql, [gainid]);
      if (result && result[0] && result[0].email_confirmed) {
        return true;
      } else if (!result[0]) {
        return true;
      }
      return false;
    } catch (err) {
      throw new Error(err.message);
    }
  },
  getBalanceByGainId: async function (gainid) {
    try {
      const result = await connection.query(`SELECT balance FROM users WHERE gainid = ?`, [gainid]);
      assert(result.length === 1);
      return result[0].balance;
    } catch (err) {
      throw new Error(err.message);
    }
  },
  earnedEnoughToWithdraw: async function (gainid) {
    try {
      const result = await connection.query(
        `
      SELECT SUM(coins) AS coins FROM (
        SELECT SUM(coins) AS coins FROM surveys WHERE gainid = ?
          UNION ALL
        SELECT SUM(coins) AS coins FROM videos WHERE gainid = ?
          UNION ALL
        SELECT SUM(coins) AS coins FROM refearnings WHERE gainid = ?
      ) AS f
    `,
        [gainid, gainid, gainid]
      );
      if (result[0].coins && result[0].coins >= config.withdraw.minEarnedToWithdraw) return true;

      return false;
    } catch (err) {
      throw new Error(err.message);
    }
  },
  updateBalanceByGainId: async function (gainid, amount) {
    try {
      const result = await connection.query(
        `UPDATE users SET balance = balance + ? WHERE gainid = ?`,
        [amount, gainid]
      );
      assert(result.affectedRows === 1);
      const secondResult = await connection.query(
        `INSERT INTO balance_movements (gainid, amount, new_balance) VALUES (?, ?, (SELECT balance FROM users WHERE gainid = ?));`,
        [gainid, amount, gainid]
      );
      assert(secondResult.affectedRows === 1);
      return null;
    } catch (err) {
      throw new Error(err.message);
    }
  },
  insertPendingSiteGiftCardWithdraw: async function (
    gainid,
    coinAmount,
    cardType,
    countryCode,
    date,
    warningMessage
  ) {
    let sql = `
      INSERT INTO pendingwithdraw (gainid, date, warning_message) VALUES (?, ?, ?);
      INSERT INTO pending_site_gift_card_withdraw (releaseid, coinamount, card_type, date, country_code) VALUES
      (LAST_INSERT_ID(), ?, ?, ?, ?);
    `;
    try {
      const result = await connection.query(sql, [
        gainid,
        date,
        warningMessage,
        coinAmount,
        cardType,
        date,
        countryCode,
      ]);
      assert(result.length === 2);
      return result;
    } catch (err) {
      throw new Error(err.message);
    }
  },
  insertSiteGiftCardWithdrawal: async function (
    gainid,
    coinAmount,
    cardCode,
    cardType,
    countryCode,
    date,
    approver
  ) {
    let sql = `
      INSERT INTO withdraw (gainid, date, approver) VALUES (?, ?, ?);
      INSERT INTO site_gift_card_withdraw (withdrawid, coinamount, card_code, card_type, date, country_code) VALUES
      (LAST_INSERT_ID(), ?, ?, ?, ?, ?)
    `;
    try {
      const result = await connection.query(sql, [
        gainid,
        date,
        approver,
        coinAmount,
        cardCode,
        cardType,
        date,
        countryCode,
      ]);
      assert(result.length === 2);
      return result;
    } catch (err) {
      throw new Error(err.message);
    }
  },
  isUserVerified: function (gainid) {
    const sql = `
        SELECT verified FROM users WHERE gainid = ?`;
    try {
      const result = await connection.query(sql, [gainid]);
      if (result && result[0] && result[0].verified) {
        return true;
      } else if (!result[0]) {
        return true;
      }
      return false;
    } catch (err) {
      throw new Error(err.message);
    }
  },
};

module.exports = db;
