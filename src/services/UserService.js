import { throwHandledError } from '../lib/errorHandler';
import { balanceValidate, standingValidate } from '../validators/UserValidator';
const db = require('../database');

const minEarnText = `You must earn at least ${config.withdraw.minEarnedToWithdraw} coins ($${(
  config.withdraw.minEarnedToWithdraw / 1000
).toFixed(
  2
)}) through the offer walls before withdrawing.<br>This is to prevent abuse of the site bonuses. Please contact staff with any questions.`;

export default class UserService {
  getUserData = (userid) => {
    return await Promise.all([
      db.getAccountStanding(userid),
      db.isDefaultUserEmailVerified(userid),
      db.getBalanceByGainId(userid),
      db.earnedEnoughToWithdraw(userid),
    ]);
  };

  userValidate = (userid, coinAmount) => {
    [this.standing, this.isEmailVerified, this.balance, this.earnedEnoughBool] =
      await this.getUserData(userid);
    if (!standingValidate(this.standing))
      throwHandledError(
        `You are currently banned from withdrawing, please contact staff if you believe this is a mistake.`
      );
    if (!this.isEmailVerified)
      throwHandledError(`You must verify your E-mail address before requesting a withdrawal!`);
    if (!balanceValidate(this.balance, coinAmount))
      throwHandledError(`You don't have enough balance!`);
    if (!this.earnedEnoughBool) throwHandledError(minEarnText);
  };

  isUserVerified = (socketuser) => {
    return await db.isUserVerified(socketuser.gainid);
  };
}
