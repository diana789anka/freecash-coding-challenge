import { throwHandledError } from '../lib/errorHandler';
import {
  coinAmountValidate,
  countryCodeValidate,
  typeValidate,
} from '../validators/WithdrawValidator';
const db = require('../database');

export default class WithdrawService {
  constructor(data) {
    this.type = data.type;
    this.coinAmount = parseInt(data.coinAmount);
    this.countryCode = data.countryCode || 'WW';
  }

  withdrawValidate = (socketuser) => {
    if (!typeValidate(this.type)) throwHandledError(`An error occurred. Please try refreshing.`);
    if (!countryCodeValidate(this.countryCode))
      throw throwHandledError(`An error occurred. Please try refreshing.`);
    if (!socketuser) throwHandledError(`Please login to withdraw!`);
    if (!coinAmountValidate(this.coinAmount)) throwHandledError(`Please select an amount!`);
  };

  notVerified = (socketuser) => {
    try {
      await db.updateBalanceByGainId(socketuser.gainid, this.coinAmount * -1);
      await db.insertPendingSiteGiftCardWithdraw(
        socketuser.gainid,
        this.coinAmount,
        this.type,
        this.countryCode,
        utils.getIsoString(),
        null
      );
    } catch (err) {
      throwHandledError(`An error occurred, please try again`);
    }
  };

  verfied = (giftcardCode, date) => {
    try {
      await db.insertSiteGiftCardWithdrawal(
        socketuser.gainid,
        this.coinAmount,
        giftcardCode,
        this.type,
        this.countryCode,
        date
      );
    } catch (err) {
      throwHandledError(`An error occurred, please try again`);
    }
  };
}
