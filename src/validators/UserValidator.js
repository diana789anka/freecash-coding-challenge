export function standingValidate(standing) {
  return !(standing.banned || standing.frozen);
}

export function balanceValidate(balance, coinAmount) {
  return balance > coinAmount;
}
