import config from '../config';

export function typeValidate(type) {
  return config.validateTypes.includes(type);
}

export function countryCodeValidate(countryCode) {
  return config.validateCountryCodes.includes(countryCode);
}

export function coinAmountValidate(coinAmount) {
  return !(isNaN(coinAmount) || !coinAmount);
}
